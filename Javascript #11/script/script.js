"use strict";
// События клавиатуры не рекомендуется использовать при работе с полями ввода, потому что текст в поле ввода может быть вставлен по средствам копирования + некоторые мобильные устройства не генерируют keypress/keydown, а сразу вставляют текст в поле.

let btn = document.querySelectorAll("button");
const enter = document.getElementById("enter");
const s = document.getElementById("s");
const e = document.getElementById("e");
const o = document.getElementById("o");
const n = document.getElementById("n");
const l = document.getElementById("l");
const z = document.getElementById("z");

document.addEventListener("keydown", function (event) {
  if (event.code == "Enter") {
    enter.classList.add("blue");
    s.classList.remove("blue");
    e.classList.remove("blue");
    o.classList.remove("blue");
    n.classList.remove("blue");
    l.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyS") {
    s.classList.add("blue");
    enter.classList.remove("blue");
    e.classList.remove("blue");
    o.classList.remove("blue");
    n.classList.remove("blue");
    l.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyE") {
    e.classList.add("blue");
    s.classList.remove("blue");
    enter.classList.remove("blue");
    o.classList.remove("blue");
    n.classList.remove("blue");
    l.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyO") {
    o.classList.add("blue");
    s.classList.remove("blue");
    e.classList.remove("blue");
    enter.classList.remove("blue");
    n.classList.remove("blue");
    l.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyN") {
    n.classList.add("blue");
    s.classList.remove("blue");
    e.classList.remove("blue");
    o.classList.remove("blue");
    enter.classList.remove("blue");
    l.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyL") {
    l.classList.add("blue");
    s.classList.remove("blue");
    e.classList.remove("blue");
    o.classList.remove("blue");
    n;
    n.classList.remove("blue");
    enter.classList.remove("blue");
    z.classList.remove("blue");
  } else if (event.code == "KeyZ") {
    z.classList.add("blue");
    s.classList.remove("blue");
    e.classList.remove("blue");
    o.classList.remove("blue");
    n.classList.remove("blue");
    l.classList.remove("blue");
    enter.classList.remove("blue");
  }
});
