"use strict"

// Реализовать класс Employee, в котором будут следующие свойства - name (имя), age (возраст), salary (зарплата). Сделайте так, чтобы эти свойства заполнялись при создании объекта.

// Создайте геттеры и сеттеры для этих свойств.

// Создайте класс Programmer, который будет наследоваться от класса Employee, и у которого будет свойство lang (список языков).

// Для класса Programmer перезапишите геттер для свойства salary. Пусть он возвращает свойство salary, умноженное на 3.

// Создайте несколько экземпляров обьекта Programmer, выведите их в консоль.

class Employee {
    constructor(name, age, salary){
        this.name = name,
        this.age = age,
        this.salary = salary
    }    
    
    set calcSalary(value){
        return this.salary;
    }
    get calcSalary(){
        return this.salary
    }
}

class Programer extends Employee{
    constructor(name, age, salary, lang){
    super(name, age, salary),
    this.lang = lang
    
}
set calcSalary(value){
    return this.salary;
}
get calcSalary(){
    return this.salary*3
}
}

const frontEnder = new Programer ('Sasha', 32, 2000, 'html, css, js')

console.log(frontEnder);
console.log(frontEnder.calcSalary);
//-----------------------------------------------------
const BackEnder = new Programer ('Misha', 22, 1000, 'html, css')

console.log(BackEnder);
console.log(BackEnder.calcSalary);
//-------------------------------------------------------
const webDev = new Programer ('Petro', 18, 2500, 'html, css, js, php')

console.log(webDev);
console.log(webDev.calcSalary);