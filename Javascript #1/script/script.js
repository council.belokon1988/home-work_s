// 1. Область видимости VAR ограничивается либо функцией, 
// либо скриптом и доступны за пределами блока. Из-за того что VAR 
// обрабатывается сразу при запуске скрипта, неважно какое место занимает в функции,
// кроме вложенных функций.
//   LET и CONST имеют блочную видимость и являются основным способом обьявления переменных.
//   CONST не подразмевают перезапись переменных.

// 2. VAR - имеет слишком большую область видимости,
//   что может привести к нежелательной перезаписи данных.
// Устарело с появлением ECMAScript 6.





"use strict";

let userName;
let userAge;
let middleAgeResult;

do {
  userName = prompt("Your name?", "Акакий Христодула");
} while (userName.trim() === "" || !isNaN(userName));

console.log(userName);
do {
  userAge = prompt("Your age?", "18");
} while (userAge === null || isNaN(+userAge) || userAge.trim() === "");

if (userAge < 18) {
  alert("You are not allowed to visit this website.");
} else if (userAge > 22) {
  alert("Welcome, " + userName + "!");
} else if (userAge >= 18 && userAge <= 22) {
  middleAgeResult = confirm("Are you sure you want to continue?");
  if (middleAgeResult == true) {
    alert("Welcome, " + userName + "!");
  } else alert("You are not allowed to visit this website.");
}

console.log(userAge);
