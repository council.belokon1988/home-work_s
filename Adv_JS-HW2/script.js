"use strict"
//Выведите этот массив на экран в виде списка (тег ul - список должен быть сгенерирован с помощью Javascript).
// На странице должен находиться div с id="root", куда и нужно будет положить этот список (похожая задача была дана в модуле basic).
// Перед выводом обьекта на странице, нужно проверить его на корректность (в объекте должны содержаться все три свойства - author, name, price). Если какого-то из этих свойств нету, в консоли должна высветиться ошибка с указанием - какого свойства нету в обьекте.
// Те элементы массива, которые являются некорректными по условиям предыдущего пункта, не должны появиться на странице.
const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
 
  
  document.getElementById("root").innerHTML = "<dl id='dl'></dl>"

  books.forEach((e, i) => {
      try {
          validateBook(e, i);
          document.getElementById('dl').innerHTML += `<dt>Book №${i + 1}: <dd>Author: ${e.author}</dd> <dd>Name: ${e.name}</dd> <dd>Price: ${e.price}</dd></dd></dt>`
      } catch (error) {
          console.log(error.message);
      }
  });
  
  function validateBook({author, name, price}, i) {
      if (!author) {
          throw new Error(`Book №${i + 1} has no author!`);
      }
      if (!name) {
          throw new Error(`Book №${i + 1} has no name!`);
      }
      if (!price) {
          throw new Error(`Book №${i + 1} has no price`);
      }
  }
  