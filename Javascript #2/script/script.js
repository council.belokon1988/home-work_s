"use strict";
// Циклы помогают описав несложный алгоритм выполнять последовательность действий 
// необходимое количество раз, установив для этого нужные условия, якоря или разрывы.

let userNumber
let tmp = "";

do {
  userNumber = prompt("Please, enter your favorite number");
} while (userNumber === null || isNaN(+userNumber) || userNumber.trim() === "" || userNumber == 0);
console.log(userNumber);

for (let i = 5; i <= userNumber; i = i + 5)
  console.log(tmp + i + ' ');

if (userNumber >= 1 && userNumber <= 4) {
  console.log('Sorry, no valid numbers');
}
