"use strict";
let newUser = new createNewUser();
let userAge = newUser.getAge();

function createNewUser() {
  let firstName = prompt("Firstname");
  let secondName = prompt("Secondname");
  let birthday = prompt("Enter your birthdate");

  return {
    getAge: function () {
      let age = Date.now() - Date.parse(birthday);
      return age / (1000 * 60 * 60 * 24 * 365).toFixed();
    },

    getLogin: function () {
      return firstName[0] + secondName;
    },

    getPassword: function () {
      return (
        firstName[0].toUpperCase() +
        secondName +
        parseInt(birthday.substr(6, 9))
      );
    },
  };
}

console.log(newUser.getLogin());
console.log(newUser.getPassword());
console.log(newUser.getAge());
// console.log(toDate());
// Возьмите выполненное домашнее задание номер 4 (созданная вами функция createNewUser()) и дополните ее следующим функционалом:

// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// Создать метод getAge() который будет возвращать сколько пользователю лет.
// Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).

// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.
