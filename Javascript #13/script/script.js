"use strict";
let btn = document.getElementById("themeButton");
let link = document.getElementById("themeLink");
let theme = "";
let lightTheme, darkTheme, currTheme;
btn.addEventListener("click", function () {
  ChangeTheme();
});

function ChangeTheme() {
  lightTheme = "css/main.css";
  darkTheme = "css/dark.css";

  currTheme = link.getAttribute("href");
  console.log(theme);

  if (currTheme == lightTheme) {
    currTheme = darkTheme;
    // theme = "dark";
    localStorage.setItem("currTheme", "darkTheme");
  } else {
    currTheme = lightTheme;
    // theme = "light";
    localStorage.setItem("currTheme", "lightTheme");
  }
  link.setAttribute("href", currTheme);

  theme = localStorage.getItem("currTheme");

  console.log(currTheme);
  console.log(theme);
  console.log();
}
// window.addEventListener("DOMContentLoaded", () => {
//   link = currTheme;
// });

window.addEventListener("DOMContentLoaded", () => {
  if (localStorage.getItem("currTheme") === "lightTheme") {
    link.setAttribute("href", "css/main.css");
  }
});
// link = local​Storage​.get​Item("currTheme")
