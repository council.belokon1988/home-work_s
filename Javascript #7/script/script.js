"use strict";

let cities = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
let arrList = "<ul>";
cities.forEach(function (item) {
  arrList += "<li>" + item + "</li>";
});
arrList += "</ul>";

document.write(arrList);

// document.write(ulArr.arrNames[3]);
// Реализовать функцию, которая будет получать массив элементов и выводить их на страницу в виде списка. Задача должна быть реализована на языке javascript, без использования фреймворков и сторонних библиотек (типа Jquery).

// Технические требования:

// Создать функцию, которая будет принимать на вход массив и опциональный второй аргумент parent - DOM-элемент, к которому будет прикреплен список (по дефолту должен быть document.body).
// Каждый из элементов массива вывести на страницу в виде пункта списка;
// Используйте шаблонные строки и метод map массива для формирования контента списка перед выведением его на страницу;

// Примеры массивов, которые можно выводить на экран:
//   ("hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv")
// ];
