"use strict";

let userNumber1 = +prompt("Please, enter first number for the math operation.")
let userNumber2 = +prompt("Please, enter second number for the math operation.")
let mathOperationType = prompt("Please, enter type of Math operation, you'd like to do.")
let operationResult

function mathOperationFunc() {

  if (mathOperationType === "+") {
    return userNumber1 + userNumber2
  }
  else if (mathOperationType === "-") {
    return userNumber1 - userNumber2
  }
  else if (mathOperationType === "*") {
    return userNumber1 * userNumber2
  }
  else if (mathOperationType === "/") {
    return userNumber1 / userNumber2
  }

}
console.log(mathOperationFunc()) 