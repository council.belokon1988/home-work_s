
"use strict";

const btn = document.getElementById('get_ip');
btn.addEventListener("click", getInfo);

function getInfo() {
    const URI = "https://api.ipify.org/?format=json";

    async function getIP() {
        const response = await fetch(URI);
        const data = await response.json();
        return data;
    }

    getIP()
    .then(({ip}) => {
        async function showAdress() {
            const URIAdress = "http://ip-api.com/";
            const field = "?fields=status,message,continent,country,region,city,district";
            const response = await fetch(URIAdress + "json/" + ip + field);
            const data = await response.json();
            return data;
        }

        showAdress()
        .then(({continent: Continent, country: Country, region: Region, city: City, area: Area}) => {
            const ol = document.createElement('ol');
            btn.after(ol);
            let items = {Continent, Country, Region, City, Area};

            for (let key in items) {
                const li = document.createElement('li');
                li.textContent = key + " : " + items[key];
                ol.append(li);
            }

            btn.disabled = true;
        });
    }) 
    .catch((error) => {
        console.log(error.message);
    });
}

