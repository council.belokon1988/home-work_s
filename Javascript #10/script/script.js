"use strict";
let subBtn = document.getElementById("subBtn");
let input1 = document.getElementById("pass");
let input2 = document.getElementById("passConfirm");
let form = document.getElementById("form");
let fas = document.getElementById("fas");
let icon = document.getElementById("icon");
let icon1 = document.getElementById("icon1");

function showSlash() {
  icon1.style.display = "block";
  input1.setAttribute("type", "password");
  icon2.style.display = "block";
  input2.setAttribute("type", "password");
}
function show_hide_password1(target) {
  if (input1.getAttribute("type") == "password") {
    target.style.display = "none";
    input1.setAttribute("type", "text");
  } else {
    input1.setAttribute("type", "password");
    target.style.display = "block";
  }
  return;
}

function show_hide_password2(target) {
  if (input2.getAttribute("type") == "password") {
    target.style.display = "none";
    input2.setAttribute("type", "text");
  } else {
    target.style.display = "block";
    input2.setAttribute("type", "password");
  }
  return false;
}

subBtn.addEventListener("click", function () {
  if (input1.value !== input2.value) {
    let errSpan = document.createElement("span");
    errSpan.style.color = "red";
    errSpan.textContent = "Нужно ввести одинаковые значения";
    label2.after(errSpan);
  } else {
    alert("You are welcome");
  }
});
