"use strict";
// Обработчик событий - это функция, которая срабатывает только после указанного события (действия пользователя).
let result = document.getElementById("inputPrice".value);
const button = document.getElementById("button");
const input = document.getElementById("inputPrice");
const span = document.createElement("span");
const span1 = document.createElement("span");

span.innerText = "Please enter correct price.";
span1.innerText = "Please enter correct price.";

input.addEventListener("focus", function () {
  input.style.outlineColor = "green";
});

function spanCreate() {
  span1.textContent = `Текущая цена: ${input.value}`;
  input.before(span1);
}

input.addEventListener("blur", function () {
  if (input.value < 0) {
    input.style.border = "3px solid red";
    form.insertBefore(span, input);
  } else if (input.value > 0) {
    spanCreate();
  }
});

function removeItems(e) {
  document.getElementById("inputPrice").value = "";

  // input.removeEventListener("blur", spanCreate);
  span.remove();
  span1.remove();
  button.remove();
}
console.log(button);
button.addEventListener("click", removeItems);
