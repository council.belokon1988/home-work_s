"use strict";
function tabing(evt, heroName) {
  let i, tabcontent, tabsTitle;

  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }

  tabsTitle = document.getElementsByClassName("tabsTitle");
  for (i = 0; i < tabsTitle.length; i++) {
    tabsTitle[i].className = tabsTitle[i].className.replace(" active", "");
  }

  document.getElementById(heroName).style.display = "block";
  evt.currentTarget.className += " active";
}
