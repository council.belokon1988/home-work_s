"use strict";
// 1. setTimeout() - устанавливает отсрочку до выполнения, а
// setInterval устанавливает временной промежуток между цикличным  выполнением операций.
// 2. setTimeout с нулевой задержкой выводится не моментально, т.к. браузер тратит время на выполнение функции.
// 3. clearInterval() необходимо вызывать чтобы разгружать систему.

window.onload = function () {
  document.body.classList.add("loaded_hiding");
  window.setTimeout(function () {
    document.body.classList.add("loaded");
    document.body.classList.remove("loaded_hiding");
  }, 500);
};
const images = document.querySelectorAll("img");
let stopBtn = document.getElementById("stopBtn");
const playBtn = document.getElementById("playBtn");
let intervalID;
let stopper = true;
let i = 1;
function go() {
  intervalID = setInterval(slider, 3000);
}
function slider() {
  if (i == 0) {
    images[i].style.display = "block";
  } else if (i == images.length) {
    images[i - 1].style.display = "none";
    images[0].style.display = "block";
    i = 0;
  } else {
    images[i - 1].style.display = "none";
    images[i].style.display = "block";
  }
  i++;
}
stopBtn.addEventListener("click", function asd() {
  clearInterval(intervalID);
});
go();
