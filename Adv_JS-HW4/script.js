"use strict"
const URI = "https://swapi.dev/api/films/";
function showFilms() {
    return fetch(URI)
    .then(response => response.json());
}
 
function showCharacters(peremURI) {
    return fetch(peremURI)
    .then(response => response.json());
}

showFilms()
    .then(({results}) => {
        results.forEach(({title: EpisodeName, episode_id: EpisodeNumber, opening_crawl: Summary, characters}) => {
            const ol = document.createElement('ol');
            document.body.append(ol);
            let items = {EpisodeName, EpisodeNumber, Summary};

            for (let key in items) {
                const li = document.createElement('li');
                li.textContent = key + " : " + items[key];
                ol.append(li);
            }
            displayCharacters(characters, ol);
        });
    }) 
    .catch((error) => {
        console.log(error.message);
    });

async function displayCharacters(characters, ol) {
    let arr = [];
    let arrPromises = [];
    characters.forEach((elem) => {
        const li = document.createElement("li");
        arrPromises.push(showCharacters(elem).then(({name}) => {
            li.textContent = name;
        }));
        arr.push(li);
    });

    await Promise.all([...arrPromises]);
    
    const li = document.createElement('li');
    li.textContent = "Characters : ";
    ol.append(li);
    
    const ulCharacter = document.createElement('ul');
    li.append(ulCharacter);
    ulCharacter.append(...arr);
    return false;
}

