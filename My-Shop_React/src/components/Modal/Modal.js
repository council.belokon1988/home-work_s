import React, { Component } from 'react';
import './Modal.scss';
import Button from "../Button/Button";

class Modal extends Component {
  render() {
    const {header, closeButton, text, actions, closeModal} = this.props
    
    return (
      <div className="Modal-wrapper" onClick={() => closeModal()}>
        <div className="Modal" onClick={(e) => e.stopPropagation()}>
          <div className="Modal--header">
            <p>{header}</p>
            {closeButton && <Button
                classBtn="closeBtn"
                backgroundColor="darkred"
                text=""
                handleClick={closeModal}
                />}
          </div>  
          <div className="Modal--body">
              {text.split('. ').map((elem, index) => {
                  return (
                      <p key={index}>
                          {elem}
                          {index !== (text.split('. ').length - 1) && "."}
                      </p>
                  )
              })}
          </div>
          <div className="Modal--btns-Ok-Cancel btns-Ok-Cancel">
              {actions.okButton()}
              {actions.cancelButton()}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;
