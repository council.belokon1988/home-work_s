import React, { Component } from 'react';

class Button extends Component {
  render() {
    const {backgroundColor, text, handleClick, id, classBtn} = this.props
    return (
      <div className="button">
        <button className={classBtn} style={{backgroundColor: backgroundColor}} 
                        onClick={() => handleClick({id})}>{text}</button>
      </div>
    );
  }
}

export default Button;
