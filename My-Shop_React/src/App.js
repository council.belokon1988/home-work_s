import React, { Component } from "react";
import './App.scss';
import Button from "./components/Button/Button.js";
import Modal from "./components/Modal/Modal";

const arrModalsValue = [
  {header: "Visiting this site may cause your health",
   text: "If you don't cherish it, click OK",
   closeButton: true,
   buttonText: {
     okBtn: "Yes, I'm crazy",
     cancelBtn: "Where's my mommy!?"
   }
  },
  {header: "Already need a help?",
   text: "Call the doctor on number 103!",
   closeButton: false,
   buttonText: {
    okBtn: "I'm done",
    cancelBtn: "I want more"
  }
  }
]

class App extends Component {
  state = {
    currentModal: undefined,
    isActive: false
  }

  openModal = ({id}) => {
    this.setState({ currentModal : arrModalsValue[id]})
    this.setState({ isActive : true})
  }

  closeModal = () => {
    this.setState({ isActive : false})
  }

  render() {
    const {currentModal, isActive} = this.state
    return (
      <div className="App">
        <div className="btnsWrapper">
          <Button
            backgroundColor="green"
            text="Open first modal"
            handleClick={this.openModal}
            id={0}
            classBtn="mainBtns"
          />
          <Button
            backgroundColor="blue"
            text="Open second modal"
            handleClick={this.openModal}
            id={1}
            classBtn="mainBtns"
          />
        </div>
        
        {isActive && <Modal
          header={currentModal.header}
          text={currentModal.text}
          closeButton={currentModal.closeButton}
          actions={{
            okButton: () => (<Button 
            text={currentModal.buttonText.okBtn}
            handleClick={this.closeModal}
            classBtn="btns-Ok-Cancel--btn"/>),

            cancelButton: () => (<Button
              text={currentModal.buttonText.cancelBtn}
              handleClick={this.closeModal}
              classBtn="btns-Ok-Cancel--btn"/>)
            }}
          isActive={isActive}
          closeModal={this.closeModal}
        />}
      </div>
    );
  }
}

export default App;
